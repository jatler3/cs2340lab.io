% Functional Error Handling

## What's right with exceptions?

Exceptions provide

- a way to consolidate error handling code and separate it from main logic, and
- an alternative to APIs that require callers of functions to know error codes, sentinal values, or calling protocols.

We can preserve both of these advantages while avoiding the disadvantages of exceptions.

## What's wrong with exceptions?

Exceptions

- break referential transparency,
- are not type-safe, and
- functions that throw excpetions are *partial*.

Also, exception syntax is a pain.

## Exceptions break referential transparency.

```Scala
def failingFn(i: Int): Int = {
  val y: Int = throw new Exception("fail!")
  try {
    val x = 42 + 5
    x + y
  } catch {
    case e: Exception => 43
  }
}
```

If `y` were referentially transparent, then we should be able to substitute the value it references:

```Scala
def failingFn2(i: Int): Int = {
  try {
    val x = 42 + 5
    x + ((throw new Exception("fail!")): Int)
  } catch {
    case e: Exception => 43
  }
}
```

But `failingFn2` returns a different result for the same input.

## Type-safety and Partiality

```Scala
def mean(xs: Seq[Double]): Double =
  if (xs.isEmpty)
    throw new ArithmeticException("mean of empty list undefined")
  else
    xs.sum / xs.length
```

`mean(Seq(1,2,3))` returns a value, but `mean(Seq())` throws an exception

- The type of the function, `Seq[Double] => Double`, does not convey the fact that an exception is thrown in some cases.
- `mean` is not defined for all values of `Seq[Double]`.

In practice, partiality is common, so we need a way to deal with it.

## Functional Error Handling in the Scala Standard Library

The Scala standard library defines three useful algebraic data types for dealing with errors:

- `Option`, which represents a value that may be absent,
- `Either`, which represents two mutually-exclusive alternatives, and
- `Try`, which represents success and failure

Note: Chapter 4 of [Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala) defines its own parallel versions of `Option` and `Either`, but we'll use the standard library versions. For a deeper understanding do the exercises in the book.

## The `Option` Type

We've seen `Option` before:

```Scala
sealed abstract class Option[+A]
final case class Some[+A](value: A) extends Option[A]
case object None extends Option[Nothing]
```

Using `Option`, `mean` becomes

```Scala
def mean(xs: Seq[Double]): Option[Double] =
  if (xs.isEmpty) None
  else Some(xs.sum / xs.length)
```


## `Option` Idioms

`Option` defines many methods that mirror methods on [`Traversable`](https://www.scala-lang.org/api/2.12.8/scala/collection/Traversable.html)s.


```Scala
sealed abstract class Option[+A] {
  def isEmpty: Boolean
  def get: A
  final def getOrElse[B >: A](default: => B): B =
    if (isEmpty) default else this.get
  final def map[B](f: A => B): Option[B] =
    if (isEmpty) None else Some(f(this.get))
  final def flatMap[B](f: A => Option[B]): Option[B] =
    if (isEmpty) None else f(this.get)
  final def filter(p: A => Boolean): Option[A] =
    if (isEmpty || p(this.get)) this else Nones
}
```

The key consequence is that you can treat `Option` as a collection, leading to many idioms for handling optional values.

## Options and Eithers versus Exceptions

Rule of thumb: only throw exceptions exceptions in cases where the program could not recover from the exception by catching it.
