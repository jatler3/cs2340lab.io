---
layout: default
title: CS2340 - Code
---

# Example Code

## <a name="basics" />Basics

- [values-variables.sc](code/basics/values-variables.sc)
- [control-structures.sc](code/basics/control-structures.sc)
- [function-basics.sc](code/basics/function-basics.sc)
- [classes-objects.sc](code/basics/classes-objects.sc)
- [functional-abstraction.sc](code/basics/functional-abstraction.sc)
- [oop.sc](code/basics/oop.sc)
- [case-classes.sc](code/basics/case-classes.sc)

## <a name="advanced" />Advanced

- [generics.sc](code/advanced/generics.sc)
- [implicits.sc](code/advanced/implicits.sc)
- [for-expressions.sc](code/advanced/for-expressions.sc)


## <a name="fp" />Functional Programming

- [fp-basics.sc](code/fp/fp-basics.sc)
- [fp-data-structures.sc](code/fp/fp-data-structures.sc)
