def failingFn(i: Int): Int = {
  val y: Int = throw new Exception("fail!")
  try {
    val x = 42 + 5
    x + y
  } catch {
    case e: Exception => 43
  }
}

// Same as failingFn but "value" of y substituted
// for y
def failingFn2(i: Int): Int = {
  try {
    val x = 42 + 5
    x + ((throw new Exception("fail!")): Int)
  } catch {
    case e: Exception => 43
  }
}

// If y were referentially transparent, this would hold,
// but it doesn't:

failingFn(1) == failingFn2(1)

def mean(xs: Seq[Double]): Double =
  if (xs.isEmpty)
    throw new ArithmeticException("mean of empty list undefined")
  else
    xs.sum / xs.length

mean(Seq(1,2,3))
mean(Seq())
