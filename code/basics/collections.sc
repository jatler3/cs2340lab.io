
// High-level traits have companion class constructors
Iterable("x", "y", "z")
List(1, 2, 3)
Vector(1, 2, 3)
Seq(1.0, 2.0)
IndexedSeq(1.0, 2.0)
Set(1, 2, 3)
Map("x" -> 24, "y" -> 25, "z" -> 26)


