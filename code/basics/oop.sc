abstract class Person(val name: String) {
  def id: Int
  override def toString = s"${getClass.getName}[name=$name]"
}

class SecretAgent(val id: Int, codename: String)
    extends Person(codename) {
  override val name = "secret" // Don’t want to reveal name . . .
  override val toString = "secret" // . . . or class name
}

class Item(val description: String, val price: Double) {
  final override def equals(other: Any) = other match {
    case that: Item => description == that.description && price == that.price
    case _ => false
  }
  final override def hashCode = (description, price).##
}

// Companion object must be in same source file as class
object Item {
  // Factory method for creating
  def apply(description: String, price: Double): Item =
    new Item(description, price)
}

val item = Item("Key Lime", 3.14)

// Traits

trait Logger {
  def log(msg: String)
}

trait ConsoleLogger extends Logger {
  def log(msg: String) { println(msg) }
}

abstract class SavingsAccount(var balance: Int) extends Logger {
  def withdraw(amount: Int) {
    if (amount > balance) log("Insufficient funds")
    else balance -= amount
   }
}

trait Timestamping extends ConsoleLogger {
  override def log(msg: String) = super.log(s"${java.time.Instant.now()} $msg")
}
trait Shortening extends ConsoleLogger {
  override def log(msg: String) =
    super.log( if (msg.length <= 18) msg else s"${msg.substring(0, 10)}")
}
trait Shouting extends Logger {
  abstract override def log(msg: String) =
    super.log(msg.toUpperCase)
}



val acct1 = new SavingsAccount(1) with Timestamping with Shortening
acct1.withdraw(2)

val acct2 = new SavingsAccount(1) with Shortening with Timestamping
acct2.withdraw(2)

val acct3 = new SavingsAccount(1) with Shortening with Shouting
acct3.withdraw(2)

// Below won't compile because there is no concrete log method
// for Shouting.log to call
//val acct4 = new SavingsAccount(1) with Shouting with Shortening
//acct4.withdraw(2)
