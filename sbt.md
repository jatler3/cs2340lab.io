---
layout: default
title: CS2340 - SBT
---


Download from [https://www.scala-sbt.org/](https://www.scala-sbt.org/).

# Scala Style Plugin

sbt is extended via plugins.  In all of the projects you write for this course, add the [Scalastyle](http://www.scalastyle.org/) plugin by following the *Setup* and *Usage* steps on the [Scalastyle SBT plugin page](http://www.scalastyle.org/sbt.html).
