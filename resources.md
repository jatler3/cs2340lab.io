---
layout: default
title: CS2340 - Resources
---

# Resources

## Get Started

>NOTE: At many points in these instructions you are told to open the "Command Prompt"/"Command Line" (Windows) or "Terminal" Mac OS X/Linux. You will be using this program a lot so you should get familiar with it pin it to your taskbar/dock.
> - On Mac open the Applications folder, then the Utilities folder, then open Terminal.app.
> - On Linux the Terminal can by opened with the keyboard shortcut "ctrl-alt-t".
> - On Windows open the start menu and search for "cmd" then click on the Command Prompt.

Complete these steps during the first week of class!

- [Install Scala](install-scala.html).
- Install [SBT](sbt.html)
- Install [IntelliJ](intellij.html)
- Install [Git](git.html).
- You should also choose and install a <a href="text-editors.html" target="_blank">programmer's text editor</a>.

## Get Help

- If you have general questions about course content or homework clarifications please post your questions on our <a href="http://piazza.com/gatech/" target="_blank">Piazza site</a>. Be sure not to post homework code for other students to see and don't post screen shots of text.
- Visit the CS2340 TA Lab in CoC ...! TAs hold [office hours](office-hours.html) there to answer questions and address any concerns you may have. Also, the TA Lab will be your resource for picking up tests you missed in recitation, submitting regrade requests etc.
- If you find broken links on slides or lecture notes you can look at <a href="https://gitlab.com/cs2340/cs2340.gitlab.io" target="_blank">the GitLab repo of this class web site</a>.  You can also see all the example projects we discuss in class on <a href="https://gitlab.com/cs2340/" target="_blank">the CS2340 GitLab group page.</a>

## Learn About General Computing

- <a href="http://matt.might.net/articles/basic-unix/" target="_blank">Basic Unix</a> - a tutorial introduction to the Unix command line that will give you the basic skills you need for this class should you choose to use a unix operating system like Linux or macOS
- <a href="https://www.computerhope.com/issues/chusedos.htm" target="_blank">Windows command line tutorial</a>
- <a href="https://technet.microsoft.com/en-us/library/bb490890.aspx" target="_blank">Windows command line reference</a>
- [Customization Tips](customization-tips.html) for the bash shell, Atom text editor, and Sublime Text editor.

## Learn About Scala

- <a href="https://docs.scala-lang.org/" target="_blank">Scala Documentation</a>
- <a href="https://twitter.github.io/scala_school/" target="_blank">Twitter Scala School</a> (Twitter is a major user of Scala.)
- Play with [example code](code.html)
